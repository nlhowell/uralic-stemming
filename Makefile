# Copyright 2018 Nick Howell
# SPDX-License-Identifier: AGPL-3.0-or-later

GIELLA_MRJ=../giella-mrj
MRJ_FST = $(GIELLA_MRJ)/tools/mt/apertium/analyser-mt-apertium-desc.und.hfstol

inputs = wiki.alpha
models = wiki.model giella

tasks = vocab-reduce dictionary
analysis: analysis-dictionary analysis-vocab-reduce
sources = mrj-rus.xml mrjwiki-pages-articles.xml.bz2 

$(sources): %:
	git annex get $@

analysis-dictionary: $(patsubst %,%.lookup,$(inputs)) $(foreach model,$(models),$(patsubst %,%.$(model).seg.lookup,$(inputs)))
	@echo "dictionary task: (words, hits, hits/words, hits/baseline)"
	@for input in $(inputs); do \
		n_input=$$(wc -l <$$input.lookup); \
		n_match=$$(grep -c -v '+?' <$$input.lookup); \
		echo "$$input $$n_match $$(echo "scale = 5; 1 - $$n_match/$$n_input" | bc) 1"; \
		for model in $(models); do \
			n=$$(grep -c -v '+?' <$$input.$$model.seg.lookup); \
			echo "$$input.$$model $$n $$(echo "scale = 5; 1 - $$n / $$n_input" | bc) $$(echo "scale = 5; $$n / $$n_match" | bc)"; \
		done; \
	done
	@echo

analysis-vocab-reduce: $(inputs) $(foreach model,$(models),$(patsubst %,%.$(model).seg,$(inputs)))
	@echo "vocab reduction: (uniq words, uniq words/baseline)"
	@for input in $(inputs); do \
		n_input=$$(sort -u $$input | wc -l); \
		echo "$$input $$n_input 1"; \
		for model in $(models); do \
			n=$$(sort -u $$input.$$model.seg | wc -l); \
			echo "$$input.$$model $$n $$(echo "scale = 5; $$n / $$n_input" | bc)"; \
		done \
	done
	@echo

WikiExtractor.py:
	wget https://svn.code.sf.net/p/apertium/svn/trunk/apertium-tools/WikiExtractor.py
	
mrj-vocab: mrj-rus.xml xmldict-extract
	./xmldict-extract mrj lg < $< | sed -e 's/([^)]*)//g' -e 's/[,.;:]/\n/g' -e 's/\s/\n/g' | grep . > $@
mrj-vocab.hfstol: mrj-vocab
	hfst-strings2fst -j $< | hfst-fst2fst -O > $@
wiki.txt: mrjwiki-pages-articles.xml.bz2 WikiExtractor.py
	python WikiExtractor.py --infn $< > $@
wiki.words: wiki.txt
	sed 's/\b/\n/g' $< | grep '\S' > $@
wiki.alpha: wiki.words
	grep '^[[:alpha:]]*$$' $< > $@
%.model: %.txt
	morfessor-train -s $@ $<
%.wiki.model.seg: wiki.model %
	morfessor-segment -l $^ | cut -f1 -d' ' | grep . > $@
%.giella.seg: % $(MRJ_FST)
	hfst-lookup -b 0 -n 1 -I $< -i $(MRJ_FST) | cut -f2 | cut -f1 -d\< | sed 's/+?//' | grep . > $@
%.lookup: % mrj-vocab.hfstol
	hfst-lookup -n 1 -I $< -i mrj-vocab.hfstol | grep '\S' > $@
	
